// Bouton de choix de peau

let btnTresClaire = document.getElementById("btnTresClaire");
let btnClaire = document.getElementById("btnClaire");
let btnMate = document.getElementById("btnMate");
let btnBrune = document.getElementById("btnBrune");
let btnNoir = document.getElementById("btnNoir");

let peau = document.getElementById("text-peau");

let iuv = document.getElementById("inputUV").value;
let photo = document.getElementById("inputPhoto").value;


btnTresClaire.addEventListener("click", () => {
    if (iuv != 0) {
        let temps = 67 / iuv;
        temps = Math.round(temps);
        peau.innerHTML = "Avec une peau très claire, vous pouvez vous exposer environ " + temps + " minutes maximum sans protection !"+"<br>"+"Veuillez remettre de la crème solaire indice 50+ toutes les 2 heures.";
    } else {
        peau.textContent = "Il n'y a aucune information pour le moment";
    }
})

btnClaire.addEventListener("click", () => {
    if (iuv != 0) {
        let temps = 100 / iuv;
        temps = Math.round(temps);
        peau.innerHTML = "Avec une peau claire, vous pouvez vous exposer environ " + temps + " minutes maximum sans protection !"+"<br>"+"Veuillez remettre de la crème solaire indice 50+ toutes les 2 heures.";
    } else {
        peau.textContent = "Il n'y a aucune information pour le moment";
    }
})

btnMate.addEventListener("click", () => {
    if (iuv != 0) {
        let temps = 300 / iuv;
        temps = Math.round(temps);
        peau.innerHTML = "Avec une peau mate, vous pouvez vous exposer environ " + temps + " minutes maximum sans protection !"+"<br>"+"Veuillez remettre de la crème solaire indice 30 à 50 toutes les 2 heures.";
    } else {
        peau.textContent = "Il n'y a aucune information pour le moment";
    }
})

btnBrune.addEventListener("click", () => {
    if (iuv != 0) {
        let temps = 400 / iuv;
        temps = Math.round(temps);
        peau.innerHTML = "Avec une peau brune, vous pouvez vous exposer environ " + temps + " minutes maximum sans protection !"+"<br>"+"Veuillez remettre de la crème solaire indice 15 à 25 toutes les 2 heures.";
    } else {
        peau.textContent = "Il n'y a aucune information pour le moment";
    }
})

btnNoir.addEventListener("click", () => {
    if (iuv != 0) {
        let temps = 500 / iuv;
        temps = Math.round(temps);
        peau.innerHTML = "Avec une peau noir, vous pouvez vous exposer environ " + temps + " minutes maximum sans protection !"+"<br>"+"Veuillez remettre de la crème solaire indice 15 à 25 toutes les 2 heures.";
    } else {
        peau.textContent = "Il n'y a aucune information pour le moment";
    }

})


//---------------------------------------------------------------
//photo background plage

let background = document.getElementById("section-mesure")

background.style.backgroundImage = "url(/css/img/"+photo+")";

//---------------------------------------------------------------
// Footer en bas de page

// 100vh chrome mobile
// https://css-tricks.com/the-trick-to-viewport-units-on-mobile/
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
    // We execute the same script as before
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
});