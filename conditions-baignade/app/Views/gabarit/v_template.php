<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url("css/style_global.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("css/style_menu.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("css/style_resultDB.css"); ?>">
    <script src="<?php echo base_url("js/plage.js"); ?>" defer></script>
    <title>Conditions de baignade</title>
</head>

<body>
    <div class="page-wrapper">
        <header>
            <nav role="navigation">
                <div id="menuToggle">

                    <input type="checkbox" />

                    <span></span>
                    <span></span>
                    <span></span>

                    <ul id="menu">
                        <a href="<?php echo base_url() ?>/Caccueil">
                            <li>Accueil</li>
                        </a>
                        <?php
                        foreach ($resultAllPlage as $row) {
                        ?> <a href="<?php echo base_url() ?>/Cplage/mesure/<?php echo $row['IDplage']; ?>">
                                <li><?php echo $row['nom']; ?></li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </nav>
            <h1 class="main-title">Conditions de baignade</h1>
        </header>
        <section class="main-section">
            <?php echo $contenu; ?>
        </section>
        <footer>
            <p>© Projet BTS SNIR - Lycée Gustave Eiffel - Armentières (<a href="https://lycee-gustave-eiffel.fr/">Contact</a>) - <u><a href="/CLogin">Administration</a></u> </p>
        </footer>
    </div>

</body>

</html>