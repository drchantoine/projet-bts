<article class="main-article">

    <!-- Affichage des données de mesure venant de la base de données -> Model "Mplage" -->
    <div id="section-mesure">
        <!-- div pour afficher un fond blanc transparent sur l'image de fond -->
        <div class="contenu-mesure">
            <h2 class="titre-sec">
                <?php foreach ($resultPlage as $row) {
                    echo $row['nom'];
                } ?>
            </h2>

            <p class="desc-mesure">
                Température de l'air :
                <span class="mesure">
                    <?php
                    if ($resultAir != NULL) {
                        $row = $resultAir[0];
                        echo $row['temperature'] . " °C ";
                    } else {
                        echo "Aucune donnée !";
                    }
                    ?>
                </span>

            </p>

            <p class="desc-mesure">
                Température de l'eau :
                <span class="mesure">
                    <?php
                    if ($resultAir != NULL) {
                        $row = $resultEau[0];
                        echo $row['temperature'] . " °C ";
                    } else {
                        echo "Aucune donnée !";
                    }
                    ?>
                </span>

            </p>

            <p class="desc-mesure">
                Vitesse du vent :
                <span class="mesure">
                    <?php
                    if ($resultAir != NULL) {
                        $row = $resultVent[0];
                        echo $row['vitesse'] . " km/h";
                    } else {
                        echo "Aucune donnée !";
                    }
                    ?>
                </span>
            </p>

            <p class="desc-mesure">
                Direction du vent :
                <span class="mesure">
                    <?php
                    if ($resultAir != NULL) {
                        $row = $resultVent[0];
                        echo $row['direction'];
                    } else {
                        echo "Aucune donnée !";
                    }
                    ?>
                </span>
            </p>

            <p class="desc-mesure">
                Indice UV :
                <span class="mesure">
                    <?php
                    if ($resultUV != NULL) {
                        $row = $resultUV[0];
                        echo $row['iuv'];
                    } else {
                        echo "Aucune donnée !";
                    }
                    ?>
                </span>
            </p>
        </div>
    </div>

    <!-- Interprétation de l'UV -->
    <div class="interp-uv">

        <h2 class="titre-sec" id="titre-peau">
            Protégez vous !
        </h2>

        <div class="info-global">
            <img class="icone-info" src="<?php echo base_url("images/icones/chapeau.png"); ?>" alt="photo d'un chapeau">
            <img class="icone-info" src="<?php echo base_url("images/icones/bouteille.png"); ?>" alt="photo d'une bouteille d'eau">
            <img class="icone-info" src="<?php echo base_url("images/icones/horloge.png"); ?>" alt="photo d'indication soleil 12-16h">
            <figcaption class="legende-info">Attention au soleil entre 14h et 16h !</figcaption>
        </div>


        <h2 class="titre-sec" id="titre-peau">
            Informations personnalisées
        </h2>

        <div>
            <p>
                Voici nos conseils en fonction de votre type de peau :
            </p>
        </div>

        <button class="boutonPeau" id="btnTresClaire"><img class="img-peau" src="<?php echo base_url("images/emojis/tresClaire.png"); ?>" alt="couleur de peau très claire"></button>
        <button class="boutonPeau" id="btnClaire"><img class="img-peau" src="<?php echo base_url("images/emojis/claire.png"); ?>" alt="couleur de peau claire"></button>
        <button class="boutonPeau" id="btnMate"><img class="img-peau" src="<?php echo base_url("images/emojis/mate.png"); ?>" alt="couleur de peau mate"></button>
        <button class="boutonPeau" id="btnBrune"><img class="img-peau" src="<?php echo base_url("images/emojis/brune.png"); ?>" alt="couleur de peau brune"></button>
        <button class="boutonPeau" id="btnNoir"><img class="img-peau" src="<?php echo base_url("images/emojis/noir.png"); ?>" alt="couleur de peau noir"></button>

        <!-- Affichage des conseil en fonction des types de peau  -->

        <div id="peau" class="mess-typePeau">
            <p id="text-peau">

            </p>
        </div>

        <input id="inputUV" type="hidden" value="<?php $row = $resultUV[0];
                                                    echo $row['iuv'];
                                                    ?>">
        <input id="inputPhoto" type="hidden" value="<?php foreach ($resultPlage as $row) {
                                                        echo $row['photo'];
                                                    } ?>">

        <!-- Messages Perso de l'office de tourisme -->

        <div class="mess-perso">
            <h2 class="titre-sec">
                Événements
            </h2>
            <p>
                <?php
                if ($resultMessage != NULL) {
                    $row = $resultMessage[0];
                    echo $row['message'];
                } else {
                    echo "Aucune donnée !";
                }
                ?>
            </p>
        </div>

</article>