<?php if (session()->get('isLoggedIn') == true) : ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/Commentaire.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <!--------------------  TABLEAU D'INSERSION  --------------------->
        <form class="Tab-Ren" method="POST" action="/COffice/Ajoutmess">
            <?php $session = session(); ?>
            <h1><?php echo "Welcome back, " . $session->get('user_name') ?> <a href="/CLogin/logout"><img class="logo-login" src="images/logo-login-off.png" alt="logo de connexion"></a></h1>
            <div class="separation"></div>


            <div class="corps-formulaire">
                <div class="gauche">

                    <div class="groupe">
                        <label>Votre plage</label>
                        <select name="IdPlage" size="">
                        <option value="1">Bray-Dunes</option>
                        <option value="2">Malo-les-bains</option>
                        <option value="3">Zuydcoote</option>
                        </select>
                    </div>

                    <div class="groupe">
                        <label>Début</label>
                        <div class="sd-container">
                            <input class="sd" name="debut" type="date" required>
                        </div>
                    </div>

                    <br><br>

                    <div class="groupe">
                        <label>Fin</label>
                        <div class="sd-container">
                            <input class="sd" name="fin" type="date" required>
                        </div>
                    </div>
                </div>



                <div class="droite">
                    <div class="groupe">
                        <label>Message</label>
                        <textarea name="message" placeholder="Saisissez ici..." required></textarea>
                    </div>
                </div>
            </div>
            <br>

            <div class="pied-formulaire" style="text-align:center;">
                <button type="submit">Envoyer le message</button>
            </div>
        </form>

        <!--------------------  TABLEAU DE COMMENTAIRE  --------------------->
        <form class="Tab-info">
            <table class="table-style">
                <!-- En-tête du tableau -->
                <thead>
                    <tr>
                        <th>Plage</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                        <th>Commentaires</th>
                    </tr>
                </thead>
                <tbody>
                        <tr>
                        <?php foreach ($result as $row) {
                            for ($i=0; $i < count($resultAllPlage) ; $i++) { 
                                if ($row['IDplage']==$resultAllPlage[$i]['IDplage']) {
                                   ?>
                                <td><?= $resultAllPlage[$i]['nom'] ?></td>

                                <?php } ?>
                                <?php } ?>
                            
                            <td><?= $row['dateDebut'] ?></td>
                            <td><?= $row['dateFin'] ?></td>
                            <td><?= $row['message'] ?></td>
                                </tr>
                    <?php } 
                    ?>
                    
                </tbody>
            </table>
        </form>
    </div>

</body>

</html>
<?php else : ?>
    <meta http-equiv="refresh" content="0;URL=<?php base_url() ?>/Caccueil">
<?php endif ?>