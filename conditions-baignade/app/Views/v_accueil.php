<img class="main-img" src="<?php echo base_url("images/plage-accueil.jpeg"); ?>" alt="Photo de la plage">
<article class="main-article">
    <h2 class="titre-sec">Dunkerque</h2>
    <p>
        Surveillance des conditions de baignade
    </p>

    <blockquote class="citation">"Bienvenue chez les ch'tis !"</blockquote>

    <p>
        N'hésitez pas à faire un tour à l'office de tourisme de la commune ! <br>
        Choisissez votre plage et informez vous sur les conditions de baignade
    </p>
</article>