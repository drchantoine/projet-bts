<?php

namespace App\Controllers;

use App\Models\Mplage;

class Caccueil extends BaseController
{
    public function index()
    {
        $mPlage = new Mplage();                             //Appel du model "Mplage"
        $data['resultAllPlage'] = $mPlage->getAllPlage();   //Menu hamburger

        //Construction de la vue finale avec les $data + la vue v_accueil + le template
        $page['contenu'] = view('v_accueil',$data);
        return view('gabarit/v_template', $page);
    }
}
