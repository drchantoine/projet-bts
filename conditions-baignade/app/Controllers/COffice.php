<?php

namespace App\Controllers;

use App\Models\Mplage;
use App\Models\Mperso;

class COffice extends BaseController
{
    public function index()
    {

        $mPlage = new Mplage();
        $model = new Mperso();
        $data['result'] = $model->getAll();
        $data['resultAllPlage'] = $mPlage->getAllPlage();
        return view('VOffice', $data);
    }
    public function Ajoutmess()
    {
        $model = new Mperso();
        $nomPlage = $this->request->getPost('IdPlage');
        $debut = $this->request->getPost('debut');
        $fin = $this->request->getPost('fin');
        $message = $this->request->getPost('message');

        $data = [
            'IDplage'=> $nomPlage,
            'message' => $message,
            'dateDebut' => $debut,
            'dateFin' => $fin,
        ];

         $model->insertMess($data);
        return redirect()->to("COffice");
    }
}
