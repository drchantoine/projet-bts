<?php

namespace App\Controllers;

use App\Models\Mplage;

class Cplage extends BaseController
{
    public function mesure($prmId = null)
    {
        $mPlage = new Mplage();                                     //Appel du model "Mplage"
        $data['resultAllPlage'] = $mPlage->getAllPlage();           //Menu hamburger
        $data['resultPlage'] = $mPlage->getNomPlage($prmId);        //Titre Secondaire page Infos (nom plage)

        // Méthode du model pour récupérer chaque informations de la base de données
        $data['resultAir'] = $mPlage->getLastMesAir($prmId);        //Temp Air
        $data['resultEau'] = $mPlage->getLastMesEau($prmId);        //Temp Eau
        $data['resultVent'] = $mPlage->getLastMesVent($prmId);      //Vitesse et Orientation Vent
        $data['resultUV'] = $mPlage->getLastMesUV($prmId);          //Indice UV
        $data['resultMessage'] = $mPlage->getLastMessPerso($prmId); //Message Perso

        //Construction de la vue final avec les $data + la vue v_plage + le template
        $page['contenu'] = view('v_plage', $data);
        return view('gabarit/v_template', $page);
    }
}
