<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Models\Mplage;


class CLogin extends BaseController
{
    public function index()
    {
        helper(['form']);
        $mPlage = new Mplage();                             //Appel du model "Mplage"
        $data['resultAllPlage'] = $mPlage->getAllPlage();   //Menu hamburger
        $data['title'] = "Connexion";
        $page['contenu'] = view('VLogin', $data);
        return view('gabarit/v_template', $page);
    }
    public function auth()
    {
        $session = session();
        $model = new UserModel();

        $login = $this->request->getVar('login');
        $password = $this->request->getVar('password');

        $data = $model->where('login', $login)->first();

        if ($data) {
            $pass = $data['pass'];
            $verify_password = password_verify($password, $pass);
            if ($verify_password) {
                $ses_data = [
                    'user_id' => $data['IDuser'],
                    'user_name' => $data['nom'],
                    'isLoggedIn' => TRUE
                ];
                $session->set($ses_data);
                return redirect()-> to('/COffice');
            } else {
                $session->setFlashdata('msg', 'Wrong password');
                return redirect()->to('/CLogin');
            }
        } else {
            $session->setFlashdata('msg', 'Login not found');
             return redirect()->to('/CLogin');
        }
    }

    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('/Caccueil');
    }
}
