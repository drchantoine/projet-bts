<?php

namespace App\Models;

use CodeIgniter\Model;

class Mplage extends Model
{
    protected $table = 'plage';
    protected $primaryKey = 'IDplage';
    protected $returnType = 'array';

// Recuperation de tous les champs de la table plage pour former le menu hamburger
    public function getAllPlage()
    {
        $requete = $this->select('*');
        return $requete->findAll();
    }

// Récupération du nom de la plage en fonction de l'IDplage (Titre Secondaire des pages d'informations)
    public function getNomPlage($prmId){
        $requete = $this->select('plage.nom,plage.photo')
        ->where(['plage.IDplage' => $prmId]);
        return $requete->findAll();
    }

// Récupération du dernier enregistrement de température de l'air en fonction de l'IDplage
    public function getLastMesAir($prmId){
        $requete = $this->select('mesair.temperature')
            ->where(['plage.IDplage' => $prmId])
            ->join('mesair', 'plage.IDplage = mesair.IDplage')
            ->orderBy('mesair.date','DESC');
        return $requete->findAll();
    }

// Récupération du dernier enregistrement de température de l'eau en fonction de l'IDplage
    public function getLastMesEau($prmId){
        $requete = $this->select('meseau.temperature')
            ->where(['plage.IDplage' => $prmId])
            ->join('meseau', 'plage.IDplage = meseau.IDplage')
            ->orderBy('meseau.date','DESC');
        return $requete->findAll();
    }

// Récupération du dernier enregistrement de la vitesse et de l'orientation du vent en fonction de l'IDplage
    public function getLastMesVent($prmId){
        $requete = $this->select('mesvent.vitesse, mesvent.direction')
            ->where(['plage.IDplage' => $prmId])
            ->join('mesvent', 'plage.IDplage = mesvent.IDplage')
            ->orderBy('mesvent.date','DESC');
        return $requete->findAll();
    }

// Récupération du dernier enregistrement de l'indice UV en fonction de l'IDplage
    public function getLastMesUV($prmId){
        $requete = $this->select('mesuv.iuv')
            ->where(['plage.IDplage' => $prmId])
            ->join('mesuv', 'plage.IDplage = mesuv.IDplage')
            ->orderBy('mesuv.date','DESC');
        return $requete->findAll();
    }

// Récupération du dernier enregistrement de message pero en fonction de l'IDplage
    public function getLastMessPerso($prmId){
        $requete = $this->select('messageperso.message')
            ->where(['plage.IDplage' => $prmId])
            ->join('messageperso', 'plage.IDplage = messageperso.IDplage')
            ->orderBy('messageperso.dateDebut','ASC');
        return $requete->findAll();
    }
}
