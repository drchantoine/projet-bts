<?php

namespace App\Models;

use CodeIgniter\Model;

class Mperso extends Model
{
    protected $table = 'messageperso';
    protected $primaryKey = 'IDmess';
    protected $returnType = 'array';

    protected $allowedFields = [
        'dateDebut',
        'dateFin',
        'IDplage'
    ];

    public function getAll()
    {
        $requete = $this->select('*')->orderby("IDmess","desc");
        return $requete->findAll();
    }

    public function insertMess($data)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('messageperso');
        $builder->ignore(true)->insert($data);
    }
}